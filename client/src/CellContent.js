import React from 'react'


class CellContent extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            class: this.props.class,
            content: this.props.content,
        }
    } 

    render() {
        return <div className={this.state.class}>
            {this.state.content}
        </div>
    }
}

export default CellContent