import React from 'react'

import BattlefieldContainer from './BattlefieldContainer'


class App extends React.Component {
  render () {
    return <div>
        <BattlefieldContainer />
      </div>
  }
}

export default App;
