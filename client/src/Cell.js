import React from 'react'
import CellContent from './CellContent'


class Cell extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            cellClass: this.props.cellClass,
            contentClass: this.props.contentClass,
            content: this.props.content,
        }

        this.cellOnClick = this.cellOnClick.bind(this)
        this.getCellClass = this.getCellClass.bind(this)
        this.setCellClass = this.setCellClass.bind(this)

        this.showPlane = true
        this.clickCounter = 0
        this.planeOrientationForCellClickCounter = {
            0: null,
            1: 'up',
            2: 'right',
            3: 'down',
            4: 'left',
        }
    }

    getCellClass() {
        return this.state.cellClass
    }

    setCellClass(newCellClass) {
        let newState = {
            cellClass: newCellClass,
            contentClass: this.state.contentClass,
            content: this.state.content,
        }

        this.setState(state => newState)
    }

    cellOnClick() {
        this.clickCounter++

        if(this.clickCounter == 5) {
            this.showPlane = false
            this.clickCounter = 0
        }
        else {
            this.showPlane = true
        }

        this.props.cellOnClickCallback(
            this.props.position.line, 
            this.props.position.column,
            this.planeOrientationForCellClickCounter[this.clickCounter],
            this.showPlane,
            this.cellOnClick,
        )
    }

    render() {
        return <div className={this.state.cellClass} onClick={this.cellOnClick}>
            <CellContent content={this.state.content} class={this.state.contentClass}/>
        </div>
    }
}
    

export default Cell