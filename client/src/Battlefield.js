import React, { Fragment } from 'react'
import styled from 'styled-components'

import Cell from './Cell'


const BattlefieldContainer = styled.div`
    align-items: center;
    align-content: center;

    border-radius: 10px;

    display: flex;

    flex-wrap: wrap;
    flex-direction: row;
    justify-content: center;

    width: 520px;
    min-width: 200px;
    height: 520px;
    min-height: 300px;

    overflow: scroll;

    opacity: ${props => props.opacity}%;
    transition-property: opacity;
    transition-duration: 1.4s;
    transition-timing-function: ease-in-out;

    @media all and (max-width: 600px) {
        width: 220px;
        min-width: 200px;
        height: 220px;
        min-height: 200px;
    }
`

class Battlefield extends React.Component {
    constructor(props) {
        super(props)

        this.showPlanePlacementOnCellClick = this.showPlanePlacementOnCellClick.bind(this)
        this.getCell = this.getCell.bind(this)
        this.checkPositioningValidityOfPlanePlacement = this.checkPositioningValidityOfPlanePlacement.bind(this)
        this.userSelectsPreviouslyShownPlanePlacement = this.userSelectsPreviouslyShownPlanePlacement.bind(this)

        this.previouslyShownCellPositions = []
        this.previouslyShownCells = []
        this.previousPlaneHead = {
            line: -1,
            column: -1,
        }
        this.previousOrientation = null

        this.nrOfSelectedPlanes = 0
        this.currentPlaneColorClass = {
            1: 'first-selected-ally-cell',
            2: 'second-selected-ally-cell',
            3: 'third-selected-ally-cell',
        }

        for(var i = 1; i <= 11; i++) {
            for(var j = 1; j <= 11; j++) {
                this['cell-' + i + '-' + j] = React.createRef()
            }
        }
    }

    getUpperPlaneCellPositions(line, column) {
        return [
            [line, column],
            [line - 1, column], [line - 1, column + 1], [line - 1, column + 2], // wing
            [line - 1, column - 1], [line - 1, column - 2], // wing
            [line - 2, column], // tail neck
            [line - 3, column], [line - 3, column - 1], [line - 3, column + 1]  // tail
        ]
    }

    getLowerPlaneCellPositions(line, column) {
        return [
            [line, column],
            [line + 1, column], [line + 1, column + 1], [line + 1, column + 2],
            [line + 1, column - 1], [line + 1, column - 2],
            [line + 2, column],
            [line + 3, column], [line + 3, column - 1], [line + 3, column + 1]
        ]
    }

    getLeftPlaneCellPositions(line, column) {
        return [
            [line, column],
            [line, column - 1], [line - 1, column - 1], [line - 2, column - 1],
            [line + 1, column - 1], [line + 2, column - 1],
            [line, column - 2],
            [line, column - 3], [line - 1, column - 3], [line + 1, column - 3],
        ]
    }

    getRightPlaneCellPositions(line, column) {
        return [
            [line, column],
            [line, column + 1], [line - 1, column + 1], [line - 2, column + 1],
            [line + 1, column + 1], [line + 2, column + 1],
            [line, column + 2],
            [line, column + 3], [line - 1, column + 3], [line + 1, column + 3],
        ]
    }

    userSelectsPreviouslyShownPlanePlacement(currentPosition, previousPlaneHead, previouslyShownCellPositions) {
        let currentLine = currentPosition[0]
        let currentColumn = currentPosition[1]

        // clicks on head -> user wants to try a new placement
        if((currentLine == previousPlaneHead.line) && (currentColumn == previousPlaneHead.column)) return false

        // if currently clicked position is a part of the previously shown plane placement
        // then the user wants to select this placement as his Nth plane
        for(const previouslyShownCellPosition of previouslyShownCellPositions) {
            if((currentLine == previouslyShownCellPosition[0]) && (currentColumn == previouslyShownCellPosition[1])) return true
        }

        return false
    }

    getCell(i, j) {
        return this['cell-' + i + '-' + j].current
    }

    checkPositioningValidityOfPlanePlacement(cellPositions) {
        for(const position of cellPositions) {
            let line = position[0]
            let column = position[1]
            if(line <= 1 || column <= 1) return false
            if(line >= 12 || column >= 12) return false
            //
            let cell = this.getCell(line, column)
            if(cell.getCellClass() != 'battlefield-item') return false // having battlefield-item as only css class means the cell wasn't selected previously
        }
        return true
    }

    async showPlanePlacementOnCellClick(line, column, orientation, showPlane, newClickUntilValidCallback) {
        let planePlacementForOrientation = {
            'up': this.getUpperPlaneCellPositions(line, column),
            'down': this.getLowerPlaneCellPositions(line, column),
            'left': this.getLeftPlaneCellPositions(line, column),
            'right': this.getRightPlaneCellPositions(line, column),
        }

        if(this.userSelectsPreviouslyShownPlanePlacement([line, column], this.previousPlaneHead, this.previouslyShownCellPositions)) {
            this.nrOfSelectedPlanes++

            await this.props.lockPlaneForDeploymentCallback(this.previouslyShownCells, this.nrOfSelectedPlanes)

            // reset previous shown plane logic <=> locks the previous plane in place as selected
            this.previouslyShownCellPositions = []
            this.previouslyShownCells = []
            this.previousPlaneHead = {
                line: -1,
                column: -1,
            }

            if(this.nrOfSelectedPlanes == 3) {
                // send info that lock-in phase is complete
            }

            return
        }

        await this.previouslyShownCells.forEach(previouslyChangedCell => {
            previouslyChangedCell.setCellClass('battlefield-item')
        })

        if(showPlane) {
            let planeCells = []
            let planeCellPositions = planePlacementForOrientation[orientation]
            if(this.checkPositioningValidityOfPlanePlacement(planeCellPositions)) {
                planeCells = planeCellPositions.map(position => this.getCell(position[0], position[1]))
            } else {
                return newClickUntilValidCallback()
            }

            planeCells.forEach(cell => {
                cell.setCellClass(cell.getCellClass() + ' ' + this.currentPlaneColorClass[this.nrOfSelectedPlanes + 1])
            })

            this.previouslyShownCellPositions = planeCellPositions
            this.previouslyShownCells = planeCells
            this.previousOrientation = orientation
            this.previousPlaneHead = {
                line: line,
                column: column,
            }
        } else {
            // when display plane is false, all previous logic for the next placement will have to be empty
            this.previouslyShownCellPositions = []
            this.previouslyShownCells = []
            this.previousOrientation = null
            this.previousPlaneHead = {
                line: -1,
                column: -1,
            }
        }
    }

    render() {
        const battlefieldCells = (() => {
            var cells = []
            for(var i = 1; i <= 11; i++) {
                for(var j = 1; j <= 11; j++) {
                    var cellClass = 'battlefield-item'
                    var cellContent = ''
                    var cellContentClass = ''

                    if(i == 1 || j == 1) {
                        cellClass += ' first-row'
                        
                        if((i == 1 || j == 1) && !(i == 1 && j == 1)) {
                            var index_value
                            if(i == 1) {
                                index_value = j - 1
                            }
                            else {
                                index_value = String.fromCodePoint(65 + (i - 2))
                            }
                            cellContent = index_value
                        }
                        cellContentClass = 'edge-cells-wrapper'
                    }
                    else {
                        cellContent = <React.Fragment>
                            <div className='hit-horizontal'/>
                            <div className='hit-vertical'/>
                        </React.Fragment>
                        cellContentClass = 'hit-cells-wrapper'

                        var pos = 10 * (i - 1) + j - 1
                        if(this.props.deployments[pos] == '-1') {
                            cellContentClass += ' hit'
                            cellClass += ' hit-cell'
                        }
                        else if(this.props.deployments[pos] == '-2') {
                            cellClass += ' miss'
                        } else if(this.props.deployments[pos] == '1') {
                            cellClass += ' first-selected-ally-cell'
                        } else if(this.props.deployments[pos] == '2') {
                            cellClass += ' second-selected-ally-cell'
                        } else if(this.props.deployments[pos] == '3') {
                            cellClass += ' third-selected-ally-cell'
                        }
                    }

                    cells.push(
                        <Cell
                            key={this.props.side + '-' + 'cell-' + i + '-' + j}
                            ref={this['cell-' + i + '-' + j]}
                            side={this.props.side}
                            position={{
                                line: i, 
                                column: j
                            }}
                            cellClass={cellClass} 
                            content={cellContent} 
                            contentClass={cellContentClass}
                            cellOnClickCallback={this.showPlanePlacementOnCellClick}
                    />)
                }
            }
            return cells
        })()

        return <BattlefieldContainer opacity={this.props.opacity}>
            <React.Fragment>
                {battlefieldCells}
            </React.Fragment>
        </BattlefieldContainer>
    }
}

export default Battlefield