import React from 'react'
import styled from 'styled-components'

import Battlefield from './Battlefield'
import switchPreview from './switch_preview.png'
import switchPreviewMobile from './switch_preview_mobile.png'


const BattlefieldsContainer = styled.div`
    align-items: center;
    display: flex;
    justify-content: center;
    overflow: scroll;

    @media all and (max-width: 600px) {
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
    }
`

const Force = styled.div`
    display: flex;

    width: 550px;
    min-width: 250px;
    height: 100%;
    min-height: 350px;

    margin: 20px;

    /* flex-direction: column; */
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;

    overflow: scroll;

    position: relative;

    @media all and (max-width: 600px) {
        height: 50%;
        width: 90%;
        justify-content: flex-start;
        margin-left: 2%;
        flex-direction: column;
        opacity: 100%;
    }
`

const FlipContainer = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    width: 200px;
`
const FlipSwitch = styled.div`
    padding: 5px;
    width: 61px;
    height: 61px;
    background-image: url(${switchPreview});
    cursor: pointer;
    position: absolute;
    right: 0;

    @media all and (max-width: 600px) {
        padding: 4px;
        width: 43px;
        height: 40px;
        background-image: url(${switchPreviewMobile});
    }
`
const FlipTooltip = styled.div`
    font-size: 1rem;
    position: absolute;
    top: 70px;
    right: 10px;
    visibility: hidden;
    color: rgb(112, 194, 180);
`
const FieldHeaders = styled.div`
    font-size: 1.1rem;
    margin-bottom: 5px;

    display: flex;

    flex-wrap: wrap;
    flex-direction: row;

    width: 520px;
    min-width: 200px;
    height: 110px;
    overflow: scroll;

    position: relative;

    @media all and (max-width: 600px) {
        width: 220px;
        min-width: 200px;
        font-size: 0.7rem;
        height: 80px;
        margin-bottom: 20px;
    }
`

class BattlefieldContainer extends React.Component {
    constructor(props) {
        super(props)

        // send req to server to get all battlefield info for this session
        //

        this.state = {
            currentBattlefield: 0,
            battlefieldTitle: 'Allied forces',
            battlefieldOpacity: 100,
            alliedDeployments: [
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
            ],
            enemyDeployments: [
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0',
                '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
            ],
            side: 'allied',
        }

        this.switchBattlefield = this.switchBattlefield.bind(this)
        this.lockPlaneForDeploymentCallback = this.lockPlaneForDeploymentCallback.bind(this)
    }

    lockPlaneForDeploymentCallback(planeCells, planeNr) {
        let newAlliedDeployments = [...this.state.alliedDeployments]
        for(const planeCell of planeCells) {
            let deploymentPosition = (10 * (planeCell.props.position.line - 1) + planeCell.props.position.column - 1)
            newAlliedDeployments[deploymentPosition] = planeNr.toString()
        }

        let newState = {
            currentBattlefield: this.state.currentBattlefield,
            battlefieldTitle: this.state.battlefieldTitle,
            battlefieldOpacity: this.state.battlefieldOpacity,
            alliedDeployments: newAlliedDeployments,
            enemyDeployments: this.state.enemyDeployments,
            side: this.state.side,
        }
        this.setState(state => newState)
    }

    minimizeOpacity() {
        var newState = {
            battlefieldOpacity: 1,
            currentBattlefield: this.state.currentBattlefield,
            battlefieldTitle: this.state.battlefieldTitle,
            alliedDeployments: this.state.alliedDeployments,
            enemyDeployments: this.state.enemyDeployments,
            side: this.state.side,
        }

        this.setState(state => newState)
    }

    switchBattlefield() {
        this.minimizeOpacity()

        setTimeout(() => {
            var newState = {
                currentBattlefield: this.state.currentBattlefield,
                battlefieldTitle: this.state.battlefieldTitle,
                alliedDeployments: this.state.alliedDeployments,
                enemyDeployments: this.state.enemyDeployments,
            }
    
            newState.currentBattlefield = this.state.currentBattlefield ^ 1
            if(newState.currentBattlefield == 0) {
                newState.battlefieldTitle = 'Allied forces'
                newState.side = 'allied'
            }
            else {
                newState.battlefieldTitle = 'Enemy forces'
                newState.side = 'enemy'
            }
    
            newState.battlefieldOpacity = 100;
    
            this.setState(state => newState)
        }, 1400)
    }
    
    render() {
        return <BattlefieldsContainer>
            <Force>
                <FieldHeaders>
                        <div className='battle-info'>
                            <div className='battlefield-title'>
                                {this.state.battlefieldTitle}
                            </div>
                            <div className='space-delimiter'> 
                                Delimiting space
                            </div>
                        </div>
                        <FlipContainer>
                            <FlipSwitch className='flip-switch' onClick={this.switchBattlefield}>
                                <FlipTooltip className='flip-tooltip'> 
                                    Switch battlefield
                                </FlipTooltip>
                            </FlipSwitch>
                        </FlipContainer>
                </FieldHeaders>
                <Battlefield
                    opacity={this.state.battlefieldOpacity}
                    side={this.state.side}
                    deployments={this.state.side == 'allied' ? this.state.alliedDeployments : this.state.enemyDeployments}
                    lockPlaneForDeploymentCallback={this.lockPlaneForDeploymentCallback}
                />
            </Force>
        </BattlefieldsContainer>
    }
}

export default BattlefieldContainer