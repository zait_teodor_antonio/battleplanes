const mongoose = require('mongoose')

const sessionSchema = new mongoose.Schema({
    generatedId: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true,
    },
    phase: {
        type: String,
        required: true,
    },
    whoGoesFirstIP: String,
    playerOneIP: {
        type: String,
        required: true,
    },
    playerTwoIP: {
        type: String,
        required: true,
    },
})

module.exports = mongoose.model('Session', sessionSchema)