const mongoose = require('mongoose')

const deploymentSchema = new mongoose.Schema({
    sessionId: {
        type: String,
        required: true,
    },
    playerIP: {
        type: String, 
        required: true,
    },
    allied_battlefield: {
        type: [Number],
        required: true,
    },
    enemy_battlefield: {
        type: [Number],
        required: true,
    },
})

module.exports = mongoose.model('Deployment', deploymentSchema)