require('dotenv').config()

const express = require('express')
const app = express()
const mongoose = require('mongoose')

mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
const db = mongoose.connection
db.on('error', (error) => console.log(error))
db.once('open', () => console.log('Connected to Database'))

app.use(express.json())

const sessionsRouter = require('./routes/sessions')
const deploymentsRouter = require('./routes/deployments')

app.use('/', sessionsRouter)
app.use('/', deploymentsRouter)

app.listen(process.env.SERVER_PORT, () => console.log('Server Started on port ' + process.env.SERVER_PORT))