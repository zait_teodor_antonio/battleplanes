const crypto = require('crypto')
const express = require('express')
const router = express.Router()

const Deployment = require('../models/deployment')

router.get('/deployment', (req, res) => {
    res.send(req.connection.remoteAddress)
})

router.post('/deployment', (req, res) => {
    var deployment = req.body
    deployment.playerIP = req.connection.remoteAddress

    if(deployment.line == undefined || deployment.column == undefined) {
        res.send("You're in the wrong neighbourhood")
        return
    }
    
    res.send('Deployment position registered')
})

module.exports = router