const crypto = require('crypto')
const express = require('express')
const router = express.Router()

const Session = require('../models/session')

router.get('/session/generate_id', (req, res) => {
    var ipAddress = req.connection.remoteAddress
    // check db if current IP already has an active session

    //

    var randomSessionId = crypto.randomBytes(48).toString('hex')
    // create new document in the sessions collection
    //
})

router.get('/session/:id', (req, res) => {
    var ipAddress = req.connection.remoteAddress

    // check if there is a session in the db with that id and fetch it 
    // check session state and phase
    // and depending on those variables, send the appropiate answer:
    //  case 1: session is pending for finding an opponent then only send out
    // 
    //  case 2: session is in active state and in lock-in phase then only send out
    //
    //  case 3: session is in active state and in battle phase then send the 2 arrays
    // of length 100(200 integers in total => )
    //

    res.send({
        allied: [],
        enemy: [],
    })
})

router.post('/session:id', (req, res) => {
    res.send(`Your id is ${req.params.id}`)
    // query the sessions collection within the battleplanes db
    // for document containing session data for this session id
    sessionInfo = req.body
    sessionState = sessionInfo.state

})

module.exports = router